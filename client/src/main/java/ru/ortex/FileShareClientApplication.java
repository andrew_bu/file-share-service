package ru.ortex;

import org.apache.log4j.Logger;
import ru.ortex.commands.Command;

import java.io.File;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class FileShareClientApplication {
  private final static Logger logger = Logger.getLogger(FileShareClientApplication.class);

  public static void main(String[] args) {

    String filesPath = Config.properties.getProperty("files.path");
    File file = new File(filesPath);
    file.mkdirs();
    if (!file.exists() || !file.isDirectory()) {
      System.err.println(file + " must exist and be a directory");
      logger.error(file + " must exist and be a directory");
      return;
    }

    System.out.println("Client has been launch");
    Scanner scanner = new Scanner(System.in);
    CommandParser parser = new CommandParser();

    while (true) {
      System.out.println("> ");
      String commandPhrase;
      try {
        commandPhrase = scanner.nextLine();
      } catch (NoSuchElementException e) {
        break;
      }
      if ("exit".equals(commandPhrase)) {
        break;
      }
      Command command = parser.parse(commandPhrase);
      command.setOutput(System.out);
      command.execute();

    }
  }
}
