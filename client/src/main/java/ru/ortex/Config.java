package ru.ortex;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Config {
  private final static Logger logger = Logger.getLogger(Config.class);

  public final static Properties properties = new Properties();
  static {
    InputStream input = Config.class.getClassLoader().getResourceAsStream("config.properties");
    try {
      properties.load(input);
    } catch (IOException e) {
      logger.error("error on load properties", e);
      throw new RuntimeException(e);
    }
  }
}
