package ru.ortex;

import ru.ortex.commands.*;

public class CommandParser {

  public Command parse(String commandPhrase) {
    String[] parts = commandPhrase.split(" ");
    String command = parts[0];

    if (parts.length == 1) {
      if (command.equals("help")) {
        return new HelpCommand();
      }

      if (command.equals("list")) {
        return new ListFilesCommand();
      }
    }

    if (parts.length == 2) {
      if (command.equals("download")) {
        return new FileDownloadCommand(parts[1]);
      }
    }

    return new PrintMessageCommand("Incorrect command. For view list available commands print 'help'");
  }
}
