package ru.ortex.connector;

import ru.ortex.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;

public class Response {

  public final int code;
  public final String phrase;
  public final InputStream input;

  public Response(int code, String phrase, InputStream input) {
    this.code = code;
    this.phrase = phrase;
    this.input = input;
  }

  public String getBodyAsString() throws IOException {
    String body = IOUtils.toString(input);
    input.close();
    return body;
  }

  public void writeBodyToFile(Path path) throws IOException {
    IOUtils.toFile(input, path);
    input.close();
  }
}
