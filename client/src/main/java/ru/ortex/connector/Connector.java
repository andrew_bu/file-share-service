package ru.ortex.connector;

import ru.ortex.Config;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class Connector {

  private static final String host = Config.properties.getProperty("server.url");

  public Response sendRequest(String path) throws IOException {

    URL url = new URL(host + path);
    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
    connection.setRequestMethod("GET");

    int code = connection.getResponseCode();
    String message = connection.getResponseMessage();

    return new Response(code, message, connection.getInputStream());
  }
}
