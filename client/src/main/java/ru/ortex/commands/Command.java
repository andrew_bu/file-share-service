package ru.ortex.commands;

import java.io.PrintStream;

public interface Command {

  void setOutput(PrintStream output);
  void execute();
}
