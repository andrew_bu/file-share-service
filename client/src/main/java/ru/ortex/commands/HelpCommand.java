package ru.ortex.commands;

import java.io.PrintStream;

public class HelpCommand implements Command {

  private PrintStream output;

  @Override
  public void setOutput(PrintStream output) {
    this.output = output;
  }

  @Override
  public void execute() {
    output.println("Available commands:\n");
    output.println("\tlist - print list available files");
    output.println("\tdownload <file> - download file");
    output.println();
  }
}
