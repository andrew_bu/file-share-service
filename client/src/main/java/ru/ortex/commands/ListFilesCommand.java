package ru.ortex.commands;

import org.apache.log4j.Logger;
import ru.ortex.connector.Connector;
import ru.ortex.connector.Response;

import java.io.IOException;
import java.io.PrintStream;

public class ListFilesCommand implements Command {
  private final static Logger logger = Logger.getLogger(ListFilesCommand.class);

  private PrintStream output;

  @Override
  public void setOutput(PrintStream output) {
    this.output = output;
  }

  @Override
  public void execute() {

    Connector connector = new Connector();
    Response response = null;
    try {
      response = connector.sendRequest("/list");

      String body = response.getBodyAsString();

      output.println("List available files:");
      output.println(body);
      output.println();
      logger.info("available files:\n" + body);

    } catch (IOException e) {
      output.println("Error on request to server: " + e.getMessage());
      logger.error("Error on request list files¡", e);

    } finally {
      if (response != null && response.input != null) {
        try {
          response.input.close();
        } catch (IOException ignore) {
        }
      }
    }

  }
}
