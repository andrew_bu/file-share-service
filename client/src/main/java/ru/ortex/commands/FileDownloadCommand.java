package ru.ortex.commands;

import org.apache.log4j.Logger;
import ru.ortex.Config;
import ru.ortex.connector.Connector;
import ru.ortex.connector.Response;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileDownloadCommand implements Command {
  private final static Logger logger = Logger.getLogger(FileDownloadCommand.class);

  private String filename;
  private PrintStream output;

  public FileDownloadCommand(String filename) {
    this.filename = filename;
  }

  @Override
  public void setOutput(PrintStream output) {
    this.output = output;
  }

  @Override
  public void execute() {
    Connector connector = new Connector();
    Response response = null;
    try {
      response = connector.sendRequest("/file?filename=" + filename);

      Path dir = Paths.get(Config.properties.getProperty("files.path"));
      Path path = dir.resolve(filename);
      output.println("start download " + filename);
      response.writeBodyToFile(path);

      output.println("file " + filename + " downloaded as " + path);
      logger.info("file " + filename + " downloaded as " + path);

    } catch (FileNotFoundException e) {
      output.println("Error: file not found");
    } catch (IOException e) {
      logger.error("error on file download", e);
      output.println("Error: " + e.getMessage());
    } finally {
      if (response != null && response.input != null) {
        try {
          response.input.close();
        } catch (IOException ignore) {
        }
      }
    }

  }
}
