package ru.ortex.commands;

import java.io.PrintStream;

public class PrintMessageCommand implements Command {

  private final String message;
  private PrintStream output;

  public PrintMessageCommand(String message) {
    this.message = message;
  }

  @Override
  public void setOutput(PrintStream output) {
    this.output = output;
  }

  @Override
  public void execute() {
    this.output.println(message);
  }
}
