package ru.ortex;

import com.google.common.base.Preconditions;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class FileShareService {

  private final File root;

  public FileShareService(File root) {
    Preconditions.checkArgument(root.exists(), root.getName() + " not exists");
    Preconditions.checkArgument(root.isDirectory(), root.getName() + " not directory");
    this.root = root;
  }

  public List<String> getFilesNames() {
    // todo рассмотреть возможность показывать файлы из вложенных директорий
    File[] files = root.listFiles(File::isFile);
    if (files == null) {
      return Collections.emptyList();
    }
    return Arrays.stream(files).map(File::getName).collect(Collectors.toList());
  }

  public File getFile(String filename) {
    return root.toPath().resolve(filename).toFile();
  }
}
