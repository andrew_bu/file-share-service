package ru.ortex.controllers;

import ru.ortex.FileShareService;
import ru.ortex.Services;
import ru.ortex.http.Controller;
import ru.ortex.http.Request;
import ru.ortex.http.Response;
import ru.ortex.http.StringResponse;

import java.io.File;

public class FileListController implements Controller {

  @Override
  public Response handle(Request request) {
    FileShareService service = Services.getFileShareService();

    String body = String.join("\n", service.getFilesNames());
    return new StringResponse(body);
  }
}
