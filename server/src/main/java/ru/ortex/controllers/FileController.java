package ru.ortex.controllers;

import ru.ortex.FileShareService;
import ru.ortex.Services;
import ru.ortex.StatisticsService;
import ru.ortex.http.*;

import java.io.File;

public class FileController implements Controller {

  @Override
  public Response handle(Request request) {
    FileShareService fileShareService = Services.getFileShareService();
    StatisticsService statisticsService = Services.getStatisticsService();

    String filename = request.params.get("filename");
    File file = fileShareService.getFile(filename);
    if (file.exists()) {
      statisticsService.downloadFile(filename);
      return new FileResponse(file);
    }
    return new StringResponse(HttpStatus.NotFound, "file not found");
  }
}
