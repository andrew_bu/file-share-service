package ru.ortex;

import java.io.File;
import java.nio.file.Paths;

// вообще я не очень люблю такие классы со статическими методами, но написание IoC container'а выходит за рамки данного ТЗ :)
public class Services {

  private static final FileShareService fileShareService = new FileShareService(new File(Config.properties.getProperty("files.path")));

  private static final StatisticsService statisticsService = new StatisticsService(Paths.get(Config.properties.getProperty("statistics.file")));

  public static FileShareService getFileShareService() {
    return fileShareService;
  }

  public static StatisticsService getStatisticsService() {
    return statisticsService;
  }
}
