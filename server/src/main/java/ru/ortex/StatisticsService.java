package ru.ortex;

import org.apache.log4j.Logger;

import java.io.*;
import java.nio.file.Path;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;

public class StatisticsService {
  private final static Logger logger = Logger.getLogger(StatisticsService.class);

  private final Path path;

  private final Map<String, Integer> map = new ConcurrentSkipListMap<>();

  public StatisticsService(Path path) {
    this.path = path;
  }

  public void downloadFile(String filename) {
    map.compute(filename, (k, v) -> {
      if (v == null) {
        v = 0;
      }
      return v + 1;
    });
  }

  public void writeToFile() {
    File file = path.toFile();
    try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)))) {
      for (Map.Entry<String, Integer> entry : map.entrySet()) {
        writer.write(entry.getKey() + ": " + entry.getValue() + "\n");
      }
    } catch (IOException e) {
      logger.error("error on write statistics to file", e);
    }
  }
}
