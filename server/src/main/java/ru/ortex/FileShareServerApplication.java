package ru.ortex;

import org.apache.log4j.Logger;
import ru.ortex.controllers.FileController;
import ru.ortex.controllers.FileListController;
import ru.ortex.http.HttpServer;

import java.io.File;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class FileShareServerApplication {
  private final static Logger logger = Logger.getLogger(FileShareServerApplication.class);

  public static void main(String[] args) throws Throwable {

    String filesPath = Config.properties.getProperty("files.path");
    File file = new File(filesPath);
    if (!file.exists() || !file.isDirectory()) {
      System.err.println(file + " must exist and be a directory");
      logger.error(file + " must exist and be a directory");
      return;
    }

    int port = Integer.parseInt(Config.properties.getProperty("server.port"));
    int poolSize = Integer.parseInt(Config.properties.getProperty("pool.size"));
    HttpServer httpServer = new HttpServer(port, poolSize);

    httpServer.register("/list", FileListController.class);
    httpServer.register("/file", FileController.class);

    ScheduledExecutorService statisticsExecutor = Executors.newScheduledThreadPool(1);

    long delay = Long.parseLong(Config.properties.getProperty("statistics.seconds.delay"));
    statisticsExecutor.scheduleAtFixedRate(() -> Services.getStatisticsService().writeToFile(), delay, delay, TimeUnit.SECONDS);

    Runtime.getRuntime().addShutdownHook(new Thread(() -> {
      httpServer.shutdown();
      statisticsExecutor.shutdown();
    }));

    httpServer.start();
  }

}