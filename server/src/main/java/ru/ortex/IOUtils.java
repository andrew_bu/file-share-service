package ru.ortex;

import java.io.*;

public class IOUtils {

  public static String toString(InputStream inputStream) throws IOException {
    try (ByteArrayOutputStream result = new ByteArrayOutputStream()) {
      copy(inputStream, result);
      return result.toString("UTF-8");
    }
  }

  public static void copy(InputStream input, OutputStream output) throws IOException {
    byte[] buffer = new byte[1024];
    int length;
    while ((length = input.read(buffer)) != -1) {
      output.write(buffer, 0, length);
    }
  }

}
