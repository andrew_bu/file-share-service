package ru.ortex.http;

import ru.ortex.IOUtils;

import java.io.*;

public class FileResponse extends Response {

  private final File file;

  public FileResponse(File file) {
    super();
    this.file = file;
    this.setContentType("application/octet-stream");
  }

  @Override
  public Long getLength() {
    return file.length();
  }

  @Override
  public void writeTo(OutputStream output) throws IOException {
    try (InputStream input = new BufferedInputStream(new FileInputStream(file))) {
      IOUtils.copy(input, output);
    }
  }
}
