package ru.ortex.http;

import com.google.common.base.Preconditions;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Request {

  public final String path;
  public final String uri;
  public final Map<String, String> params;
  public final Map<String, String> headers;

  public Request(String requestLine, Map<String, String> headers) {
    String[] parts = requestLine.split(" ");
    Preconditions.checkState(parts.length == 3);
    this.uri = parts[1];

    int startQuery = this.uri.indexOf('?');
    if (startQuery == -1) {
      this.path = this.uri;
      this.params = Collections.emptyMap();
    } else {
      this.path = this.uri.substring(0, startQuery);
      String query = this.uri.substring(startQuery + 1);
      this.params = getParams(query);
    }

    this.headers = headers;
  }

  private HashMap<String, String> getParams(String query) {
    String[] paramsPairs = query.split("&");
    HashMap<String, String> params = new HashMap<>();

    for (String paramsPair : paramsPairs) {
      String[] nameAndVal = paramsPair.split("=");
      if (nameAndVal.length == 2) {
        params.put(nameAndVal[0], nameAndVal[1]);
      }
    }
    return params;
  }

}
