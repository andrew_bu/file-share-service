package ru.ortex.http;

import java.util.HashMap;
import java.util.Map;

public class Routes {

  private final Map<String, Class<? extends Controller>> map = new HashMap<>();


  public void register(String path, Class<? extends Controller> controller) {
    map.put(path, controller);
  }

  public Controller getInstance(String path) {
    try {
      return map.getOrDefault(path, NotFoundController.class).newInstance();
    } catch (InstantiationException | IllegalAccessException e) {
      throw new RuntimeException(e);
    }
  }

}
