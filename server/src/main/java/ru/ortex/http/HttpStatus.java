package ru.ortex.http;

public enum HttpStatus {
  Ok(200, "Ok"),
  NotFound(404, "Not Found"),
  InternalServerError(500, "InternalServerError");

  private final int code;
  private final String message;

  HttpStatus(int code, String message) {
    this.code = code;
    this.message = message;
  }

  public String toResponseLine() {
    return "HTTP/1.1 " + code + " " + message;
  }
}
