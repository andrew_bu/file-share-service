package ru.ortex.http;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;

public class StringResponse extends Response {

  private final String body;

  public StringResponse(String body) {
    this(HttpStatus.Ok, body);
  }

  public StringResponse(HttpStatus status, String body) {
    super(status);
    this.body = body;
    setContentType("text/plain");
  }

  @Override
  public Long getLength() {
    return body == null ? 0L : (long) body.length();
  }


  @Override
  public void writeTo(OutputStream output) throws IOException {
    if (body != null) {
      output.write(body.getBytes());
    }
  }

}
