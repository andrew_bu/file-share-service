package ru.ortex.http;

import org.apache.log4j.Logger;

import java.io.*;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

class ConnectionHandler implements Runnable {
  private final static Logger logger = Logger.getLogger(ConnectionHandler.class);

  private final Socket socket;
  private final BufferedReader reader;
  private final OutputStream output;
  private final Routes routes;

  public ConnectionHandler(Socket socket, Routes routes) throws Exception {
    this.socket = socket;
    this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    this.output = new BufferedOutputStream(socket.getOutputStream());
    this.routes = routes;
  }

  public void run() {
    try {
      String requestLine = readRequestLine();
      logger.info("request: " + requestLine);

      Map<String, String> headers = readInputHeaders();
      Request request = new Request(requestLine, headers);

      Controller controller = routes.getInstance(request.path);
      Response response;
      try {
        response = controller.handle(request);
      } catch (Throwable t) {
        logger.error("error on handle request", t);
        response = new StringResponse(HttpStatus.InternalServerError, t.getMessage());
      }
      render(response);

    } catch (Exception e) {
      logger.error("error on handle connection", e);
    } finally {
      try {
        socket.close();
      } catch (IOException ignored) {
      }
    }
  }

  private void render(Response response) throws IOException {
    response.setContentLength();
    output.write((response.httpStatus.toResponseLine() + "\n").getBytes());

    for (Map.Entry<String, String> entry : response.headers.entrySet()) {
      String headerLine = entry.getKey() + ": " + entry.getValue() + "\n";
      output.write(headerLine.getBytes());
    }

    output.write("\n".getBytes());
    response.writeTo(output);
    output.flush();
    output.close();
  }

  private String readRequestLine() throws IOException {
    return reader.readLine();
  }

  private Map<String, String> readInputHeaders() throws IOException {
    Map<String, String> headers = new HashMap<>();
    while (true) {
      String str = reader.readLine();
      if (str == null || str.trim().length() == 0) {
        break;
      }
      int i = str.indexOf(':');
      String name = str.substring(0, i);
      String value = str.substring(i + 1).trim();
      headers.put(name, value);
    }
    return headers;
  }


}
