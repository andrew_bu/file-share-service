package ru.ortex.http;

public interface Controller {

  Response handle(Request request);

}
