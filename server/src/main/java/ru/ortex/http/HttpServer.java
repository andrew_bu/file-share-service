package ru.ortex.http;

import org.apache.log4j.Logger;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class HttpServer {
  private final static Logger logger = Logger.getLogger(HttpServer.class);

  private final int port;
  private final Routes routes = new Routes();
  private final AtomicBoolean isShutdown = new AtomicBoolean(false);
  private final ExecutorService executorService;

  public HttpServer(int port, int poolSize) {
    this.port = port;
    this.executorService = Executors.newFixedThreadPool(poolSize);
  }

  public void register(String path, Class<? extends Controller> controller) {
    routes.register(path, controller);
  }

  public void start() throws Exception {

    ServerSocket serverSocket = new ServerSocket(port);
    logger.info("Server start! Listen: " + port);

    while (!isShutdown.get()) {
      Socket socket = serverSocket.accept();
      executorService.submit(new ConnectionHandler(socket, routes));
    }
  }

  public void shutdown() {
    isShutdown.set(true);
    try {
      executorService.awaitTermination(10, TimeUnit.SECONDS);
    } catch (InterruptedException ignore) {
    }
  }

}
