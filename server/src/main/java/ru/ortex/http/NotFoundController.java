package ru.ortex.http;

public class NotFoundController implements Controller {
  @Override
  public Response handle(Request request) {
    return new StringResponse(HttpStatus.NotFound, "Not Found");
  }
}
