package ru.ortex.http;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

public abstract class Response {

  public final HttpStatus httpStatus;
  public final Map<String, String> headers;

  protected Response() {
    this(HttpStatus.Ok);
  }

  protected Response(HttpStatus httpStatus) {
    this.httpStatus = httpStatus;

    this.headers = new HashMap<>();
    this.headers.put("Server", "FileShareServer");
  }

  protected void setContentType(String type) {
    headers.put("Content-Type", type);
  }

  protected void setContentLength() {
    headers.put("Content-Length", getLength().toString());
  }

  public abstract Long getLength();

  public abstract void writeTo(OutputStream output) throws IOException;

}
