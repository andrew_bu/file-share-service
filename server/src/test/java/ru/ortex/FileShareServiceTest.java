package ru.ortex;

import org.junit.Test;

import java.io.File;
import java.util.Arrays;

import static org.junit.Assert.*;

public class FileShareServiceTest {

  @Test
  public void getFilesNames() throws Exception {
    FileShareService fileShareService = Services.getFileShareService();

    assertTrue(fileShareService.getFilesNames().containsAll(Arrays.asList("1.txt", "2.txt")));
  }

  @Test(expected = IllegalArgumentException.class)
  public void getFilesNamesDirNotExist() {
    File dir = new File("files2");
    new FileShareService(dir);
  }

  @Test
  public void getFile() throws Exception {
    FileShareService fileShareService = Services.getFileShareService();
    File file = fileShareService.getFile("1.txt");

    assertEquals(file, TestUtils.FILES_ROOT.resolve("1.txt").toFile());
  }


}