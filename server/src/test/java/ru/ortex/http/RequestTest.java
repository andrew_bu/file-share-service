package ru.ortex.http;

import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.*;

public class RequestTest {

  @Test
  public void requestPath() {
    Request request = new Request("GET / HTTP/1.1", Collections.emptyMap());
    assertEquals(request.path, "/");
  }

  @Test
  public void requestPathAndParams() {
    Request request = new Request("GET /file?name=1.txt HTTP/1.1", Collections.emptyMap());
    assertEquals(request.path, "/file");
    assertEquals(request.params.get("name"), "1.txt");
  }

  @Test
  public void requestInvalidParams() {
    Request request = new Request("GET /file?name1.txt HTTP/1.1", Collections.emptyMap());
    assertEquals(request.params.get("name"), null);
  }
}