package ru.ortex.controllers;

import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;
import com.google.common.io.Files;
import org.junit.Test;
import ru.ortex.TestUtils;
import ru.ortex.http.HttpStatus;
import ru.ortex.http.Request;
import ru.ortex.http.Response;

import java.io.*;
import java.nio.file.Path;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

public class FileControllerTest {

  @Test
  public void fileNotFound() throws Exception {
    FileController controller = new FileController();

    Request request = new Request("GET /?filename=not_exist HTTP/1.1", Collections.emptyMap());
    Response response = controller.handle(request);

    assertEquals(response.httpStatus, HttpStatus.NotFound);
  }

  @Test
  public void downloadTxt() throws Exception {

    Request request = new Request("GET /?filename=1.txt HTTP/1.1", Collections.emptyMap());
    assertDownloadFile(request, TestUtils.FILES_ROOT.resolve("1.txt"));
  }

  @Test
  public void downloadBigTxt() throws Exception {

    Request request = new Request("GET /?filename=big.txt HTTP/1.1", Collections.emptyMap());
    assertDownloadFile(request, TestUtils.FILES_ROOT.resolve("big.txt"));
  }

  @Test
  public void downloadPdf() throws IOException {

    Request request = new Request("GET /?filename=resume.pdf HTTP/1.1", Collections.emptyMap());
    assertDownloadFile(request, TestUtils.FILES_ROOT.resolve("resume.pdf"));
  }

  private void assertDownloadFile(Request request, Path source) throws IOException {
    Response response = new FileController().handle(request);
    assertEquals(response.httpStatus, HttpStatus.Ok);

    File downloaded = downloadFile(response);

    assertEquals(md5(downloaded), md5(source.toFile()));
  }

  private File downloadFile(Response response) throws IOException {
    File tempFile = File.createTempFile("test", "");

    try (OutputStream output = new BufferedOutputStream(new FileOutputStream(tempFile))) {
      response.writeTo(output);
      output.flush();
    }
    return tempFile;
  }

  private static String md5(File file) throws IOException {
    HashCode md5 = Files.hash(file, Hashing.md5());
    return md5.toString();
  }


}