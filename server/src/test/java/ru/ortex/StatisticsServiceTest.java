package ru.ortex;

import org.junit.Assert;
import org.junit.Test;

import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.core.Is.is;

public class StatisticsServiceTest extends Assert {

  @Test
  public void statistics() throws Exception {

    Path path = Files.createTempFile("stat", ".txt");
    StatisticsService service = new StatisticsService(path);

    ExecutorService executor1 = Executors.newFixedThreadPool(1);
    for (int i = 0; i < 50; i++) {
      executor1.submit(() -> service.downloadFile("file1"));
    }

    ExecutorService executor2 = Executors.newFixedThreadPool(1);
    for (int i = 0; i < 70; i++) {
      executor2.submit(() -> service.downloadFile("file2"));
    }

    executor1.awaitTermination(100, TimeUnit.MILLISECONDS);
    executor2.awaitTermination(100, TimeUnit.MILLISECONDS);

    service.writeToFile();

    String statistics = IOUtils.toString(new FileInputStream(path.toFile()));
    String[] lines = statistics.split("\n");
    Map<String, Integer> map = new HashMap<>();
    for (String line : lines) {
      String[] pair = line.split(": ");
      map.put(pair[0], Integer.valueOf(pair[1]));
    }

    assertThat(map.get("file1"), is(50));
    assertThat(map.get("file2"), is(70));
  }

}